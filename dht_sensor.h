#include "DHT.h"

DHT dht(DHT_PIN, DHT22); // pin, type

void setupDHT()
{
  dht.begin();
}

String readTempHumid() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius
  float t = dht.readTemperature();
  
  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {

    return "Failed to read from DHT sensor!";
  }
  return  "{\"humidity\": \""+ String(h) +"\", \"temperature\": \"" +String(t) + "\"}";
}
