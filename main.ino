#include "params.h"
#include <SoftwareSerial.h>
#define SerialMon Serial
#include "dht_sensor.h"
#include "modem.h"




String termInput = "";
String modemInput = "";


String parseTermCmd(String cmd) {
  cmd.trim();
  SerialMon.println("Command: "+ cmd);
  
//  if (cmd.startsWith("/restart")) {
//    modem.restart();
//  }
  if (cmd.startsWith("/temp")) {
    SerialMon.println("Reading humid and temp...");
    return readTempHumid();
  }

  return "";
  
}

#include "mqtt.h"
#include "pir.h"

void setup() {
  SerialMon.begin(9600);               // Скорость обмена данными с компьютером
  delay(5000);
  SerialMon.println("Start!");
  restartModem();
  setupDHT();
  SIM800.begin(9600);               // Скорость обмена данными с модемом
  setupModem();
  setupMqtt();
  setupPir();
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  processTermInput();
  loopModem();
  loopPir();
  loopMqtt();
  
  blink();
  
}
void blink() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW); 
}

String processTermInput() {
  if (SerialMon.available()){

    termInput = SerialMon.readString();
    
    if (termInput.charAt(0) == '/') {
      SerialMon.println(parseTermCmd(termInput));
    }
    else {
      SIM800.print(termInput);
      while (!SIM800.available());
      SerialMon.println("modem: "+SIM800.readString());   
    }
    return termInput;
  }
    
  
  return String("");
}
