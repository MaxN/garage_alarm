#define TINY_GSM_MODEM_SIM800
#include <TinyGsmClient.h>

SoftwareSerial SIM800(MODEM_TX_PIN, MODEM_RX_PIN);        // 8 - RX Arduino (TX SIM800L), 9 - TX Arduino (RX SIM800L)

TinyGsm modem(SIM800);
//TinyGsmClientSecure client(modem);
TinyGsmClient client(modem);

#define APN "inet"
#define APN_USER ""
#define APN_PASS ""

uint32_t lastOnlineCheckAttempt = 0;

void restartModem() {
  SerialMon.print("Reseting modem pin....");
  pinMode(MODEM_RESET_PIN, OUTPUT);
  digitalWrite(MODEM_RESET_PIN, HIGH);
  delay(500);
  digitalWrite(MODEM_RESET_PIN, LOW);
  delay(500);
  digitalWrite(MODEM_RESET_PIN, HIGH);
  delay(1000);
  SerialMon.println("done.");
}

bool setupModem() {
  SerialMon.print("Initializing modem...");
//  modem.restart();
   modem.init();
  SerialMon.println('done');
  
  modem.gprsConnect(APN, APN_USER, APN_PASS);

  SerialMon.print("Waiting for network...");
  if (!modem.waitForNetwork()) {
    SerialMon.println(" fail");
    delay(1000);
    return false;
  }
  SerialMon.println(" success");

  if (modem.isNetworkConnected()) {
    SerialMon.println("Network connected");
    
  } else {
    SerialMon.println("Network is not connected");
    return false;
  }

  SerialMon.print(F("Connecting to "));
  SerialMon.print(APN);
  if (!modem.gprsConnect(APN, APN_USER, APN_PASS)) {
    SerialMon.println(" fail");
    delay(1000);
    return false;
  }
  SerialMon.println(" success");

  if (modem.isGprsConnected()) {
    SerialMon.println("GPRS connected");
  } else {
    SerialMon.println("GPRS is not connected");
    return false;
  }
  
  return true;
  
}
void loopModem() {
  uint32_t t = millis();
  if (t - lastOnlineCheckAttempt > 60000L) {
    lastOnlineCheckAttempt = t;
    if (!modem.isGprsConnected()) {
      SerialMon.println("GPRS is not connected");
      restartModem();
      setupModem();
    }
  }
}
