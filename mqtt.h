#include <PubSubClient.h>

PubSubClient mqtt(client);

unsigned long previousMillis = 0;

const long interval = 30*60*1000L; //each 5 min

void mqttLog(char *msg) {
  mqtt.publish(MQTT_LOG_TOPIC, msg);
}
void mqttLog(String msg) {
  char buf[64]={0};
  msg.toCharArray(buf, 64);
  mqttLog(buf);
}

uint32_t lastReconnectAttempt = 0;

void mqttCallback(char* topic, byte* payload, unsigned int len) {
  String value = "";
  for (int i = 0; i < len; i++) {
      value += (char) payload[i];
  }
  
  SerialMon.print("Message arrived [");
  SerialMon.print(topic);
  SerialMon.print("]: ");
  SerialMon.write(payload, len);
  SerialMon.println();

  // Only proceed if incoming message's topic matches
  if (String(topic) == MQTT_CMD_TOPIC) { 
    SerialMon.println("CMD");
    String result = parseTermCmd(value);
    SerialMon.println(result);
    mqttLog(result);
  }
}

boolean mqttConnect() {
  SerialMon.print("Connecting to ");
  SerialMon.print(MQTT_BROKER);
  SerialMon.print(":");
  SerialMon.print(MQTT_BROKER_PORT);

  // Connect to MQTT Broker
//  boolean status = mqtt.connect(MQTT_ROOT_TOPIC);

  // Or, if you want to authenticate MQTT:
  boolean status = mqtt.connect("GarageAlarm", MQTT_USER, MQTT_PASS);

  if (status == false) {
    SerialMon.println(" fail");
    return false;
  }
  SerialMon.println(" success");
  
  mqttLog("GarageAlarm connected");
  mqtt.subscribe(MQTT_CMD_TOPIC);
  return mqtt.connected();
}


void setupMqtt() {
  // MQTT Broker setup
  mqtt.setServer(MQTT_BROKER, MQTT_BROKER_PORT);
  mqtt.setCallback(mqttCallback);
  mqtt.setKeepAlive(60);
  mqtt.setSocketTimeout(30);
}

void loopMqtt() {

  if (!mqtt.connected()) {
    SerialMon.println("=== MQTT NOT CONNECTED ===");
    // Reconnect every 10 seconds
    uint32_t t = millis();
    if (t - lastReconnectAttempt > 10000L) {
      lastReconnectAttempt = t;
      if (mqttConnect()) {
        lastReconnectAttempt = 0;
      }
    }
    delay(1000);
    return;
  }

  mqtt.loop();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval  or previousMillis == 0) {
    SerialMon.print("publish temp and humid data to ");
    SerialMon.println(MQTT_DHT_PUB_TOPIC);
   
    SerialMon.print("data: ");
    String data = readTempHumid();
    char buf[128] = {0};
    data.toCharArray(buf, 128);
    SerialMon.println(buf);
    previousMillis = currentMillis;
    mqtt.publish(MQTT_DHT_PUB_TOPIC, buf);
  }
  

  

}
