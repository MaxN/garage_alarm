#ifndef PARAMS_H
#define PARAMS_H

#define MQTT_ROOT_TOPIC "GarageAlarm"
#define MQTT_CMD_TOPIC "GarageAlarm/cmd"
#define MQTT_PUB_TOPIC "GarageAlarm/data"
#define MQTT_DHT_PUB_TOPIC "GarageAlarm/dht"
#define MQTT_LOG_TOPIC "GarageAlarm/log"
#define MQTT_USER "USER"
#define MQTT_PASS "PASS"
#define MQTT_PIR_PUB_TOPIC "GarageAlarm/pir"

#define MQTT_BROKER "broker.com"
#define MQTT_BROKER_PORT 1883
#define MODEM_RESET_PIN 10
#define MODEM_TX_PIN 8
#define MODEM_RX_PIN 9
#define DHT_PIN 2
#define PIR_PIN 4
#endif
