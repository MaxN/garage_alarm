bool IsMotionDetected = false;

void setupPir() {
   pinMode(PIR_PIN, INPUT);
}

void loopPir() {
  int value = digitalRead(PIR_PIN);
  
  if (value == HIGH) {
    if (!IsMotionDetected) {
      IsMotionDetected = true;
      SerialMon.println("Motion detected");
      
      mqtt.publish(MQTT_PIR_PUB_TOPIC, "1");
    }
    
  } else {
    if (IsMotionDetected) {
      IsMotionDetected = false;
      SerialMon.println("No motion");
      mqtt.publish(MQTT_PIR_PUB_TOPIC, "0");
    }
  }
  
}
